gnome-shell-extension-flypie (27-1) unstable; urgency=medium

  * Compile and install the missing gschemas.compiled file.
    Without this file we cannot enable this extension.

 -- Mo Zhou <lumin@debian.org>  Wed, 01 Jan 2025 22:24:05 -0500

gnome-shell-extension-flypie (27-0.1) unstable; urgency=medium

  * Non-maintainer upload
  * New upstream release (Closes: #1079244)
  * Bump maximum GNOME Shell to 47

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 04 Oct 2024 19:38:40 -0400

gnome-shell-extension-flypie (26-2) unstable; urgency=high

  [ Jeremy Bícha ]
  * Fix GNOME Shell maximum version to match metadata.json (Closes: #1052096)

 -- Mo Zhou <lumin@debian.org>  Wed, 17 Jul 2024 01:41:02 -0400

gnome-shell-extension-flypie (26-1) unstable; urgency=medium

  * New upstream version 26 (Closes: #1052096)

 -- Mo Zhou <lumin@debian.org>  Mon, 15 Jul 2024 01:59:49 -0400

gnome-shell-extension-flypie (24-1~exp1) experimental; urgency=medium

  * New upstream version 24
  * Bump gnome-shell dependency to version 45. (Closes: #1052096)
  * Release to experimental.

 -- Mo Zhou <lumin@debian.org>  Sun, 24 Sep 2023 00:54:25 -0400

gnome-shell-extension-flypie (22-1) unstable; urgency=medium

  * New upstream version 22

 -- Mo Zhou <lumin@debian.org>  Fri, 08 Sep 2023 20:29:05 -0400

gnome-shell-extension-flypie (21-2) unstable; urgency=medium

  * Mark gnome-shell 44 as already supported. (Closes: #1041571).
    Thanks to Simon McVittie!

 -- Mo Zhou <lumin@debian.org>  Thu, 24 Aug 2023 14:03:20 -0400

gnome-shell-extension-flypie (21-1) unstable; urgency=medium

  * New upstream version 21

 -- Mo Zhou <lumin@debian.org>  Sun, 16 Jul 2023 15:06:00 -0700

gnome-shell-extension-flypie (17-1) unstable; urgency=medium

  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Sun, 16 Oct 2022 16:01:26 -0400

gnome-shell-extension-flypie (17-1~exp1) experimental; urgency=medium

  * Copyright: add some svg files with CC-BY-4.0 license metadata
  * New upstream version 17
  * Update d/watch file.
  ^ Remove the CC-BY-4.0 license part for some svg files.
    The corresponding license headers are removed in the new release.
    They are licensed as CC0 as described in resources/README.md
  * Bump Gnome compatibility to (<= 44~).

 -- Mo Zhou <lumin@debian.org>  Mon, 10 Oct 2022 13:32:43 -0400

gnome-shell-extension-flypie (16-1~exp1) experimental; urgency=medium

  * Initial release. (Closes: #1011460)

 -- Mo Zhou <lumin@debian.org>  Wed, 20 Jul 2022 21:36:53 -0700
